<?php
final class Emprunt {

    private $_idBooks,
            $_idUsers,
            $_dateResa,
            $_dateRetour;

    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * Get the value of _idBooks
     */
    public function getIdBooks()
    {
        return $this->_idBooks;
    }

    /**
     * Set the value of _idBooks
     *
     * @return  self
     */
    public function setIdBooks(int $_idBooks)
    {
        $this->_idBooks = $_idBooks;
    }

    /**
     * Get the value of _idUsers
     */
    public function getIdUsers()
    {
        return $this->_idUsers;
    }

    /**
     * Set the value of _idUsers
     *
     * @return  self
     */
    public function setIdUsers(int $_idUsers)
    {
        $this->_idUsers = $_idUsers;
    }

    /**
     * Get the value of _dateResa
     */
    public function getDateResa()
    {
        return $this->_dateResa;
    }

    /**
     * Set the value of _dateResa
     *
     * @return  self
     */
    public function setDateResa($_dateResa)
    {
        $this->_dateResa = $_dateResa;
    }

    /**
     * Get the value of _dateRetour
     */
    public function getDateRetour()
    {
        return $this->_dateRetour;
    }

    /**
     * Set the value of _dateRetour
     *
     * @return  self
     */
    public function setDateRetour($_dateRetour)
    {
        $this->_dateRetour = $_dateRetour;
    }



}

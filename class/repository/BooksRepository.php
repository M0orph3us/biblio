<?php 
final class BooksRepository {

    private $_db;
    private $_books;

    public function __construct()
    {
        $this->_db = new Database();
        $this->_db = $this->_db->getBDD();
    }

    public function getAll()
    {
        $sql = "SELECT * FROM books;";
        $requete = $this->_db->query($sql);
        $resultat = $requete->fetchAll(PDO::FETCH_ASSOC);

        foreach($resultat as $ligne => $infos)
        {
            // Recuperer l'ID de l'auteur dans la table relationlivresauteurs
            $relLivrAuteur = new RelLivreAuteurRepository;
            $id_auteur = $relLivrAuteur->getIdAuthor($infos["id"]);

            // Recuperer le Nom et Prénom de l'auteur
            $Authors_Repo = new AuthorsRepository();
            $infos["Author"] = $Authors_Repo->getAuthor();

            $books[$ligne] = new Books($infos);
        }
        return $books;
    }

    public function getOne()
    {
        $sql = "SELECT title FROM books WHERE id = :id ;";
        $requete = $this->_db->query($sql);
        $resultat = $requete->fetch(PDO::FETCH_ASSOC);

        return $resultat;


    }
    
}
?>
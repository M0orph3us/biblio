<?php
class Users {

    private $_id,
        $_Name,
        $_Firstname,
        $_Mail,
        $_Phone,
        $_Login,
        $_Password;

    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "get" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Les Getters
    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_Name;
    }

    public function getFirstname()
    {
        return $this->_Firstname;
    }

    public function getMail()
    {
        return $this->_Mail;
    }

    public function getPhone()
    {
        return $this->_Phone;
    }

    public function getLogin()
    {
        return $this->_Login;
    }

    public function getPassword()
    {
        return $this->_Password;
    }

    // Les Setters:
    public function setId(int $id)
    {
        $this->_id = $id;
    }

    public function setName(string $name)
    {
        $name = htmlentities($name);
        $this->_Name = $name;
    }

    public function setFirstname(string $firstname)
    {
        $firstname = htmlentities($firstname);
        $this->_Firstname = $firstname;
    }

    public function setMail(string $mail)
    {
        $mail = htmlentities($mail);
        $this->_Mail = $mail;
    }

    public function setPhone(string $phone)
    {
        $phone = htmlentities($phone);
        $this->_Phone = $phone;
    }

    public function setLogin(string $login)
    {
        $login = htmlentities($login);
        $this->_Login = $login;
    }

    public function setPassword(string $password)
    {
        $password = htmlentities($password);
        $this->_Password = ($password);
    }
}

<?php
class Authors {
    // Les attributs
    private $_id;
    private $_name;
    private $_firstname;
    private $_dateVie;

    // Constructor
    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Les méthodes, Getters/Setters

    public function getId()
    {
        return $this->_id;
    }

    /**
     * Get the value of _name
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Set the value of _name
     *
     * @return  self
     */
    private function setName(string $_name)
    {
        $this->_name = htmlentities($_name);
    }

    /**
     * Get the value of _firstname
     */
    public function getFirstname()
    {
        return $this->_firstname;
    }

    /**
     * Set the value of _firstname
     *
     * @return  self
     */
    private function setFirstname(string $_firstname)
    {
        $this->_firstname = htmlentities($_firstname);
    }

    /**
     * Get the value of _dateVie
     */
    public function getDateVie()
    {
        return $this->_dateVie;
    }

    /**
     * Set the value of _dateVie
     *
     * @return  self
     */
    private function setDateVie($_dateVie)
    {
        $this->_dateVie = htmlentities($_dateVie);
    }
}

<?php
final class Books {

    // Attributs:
    private $_id,
            $_Titre,
            $_Data_publi,
            $_Stock,
            $_Resume,
            $_Auteur,
            $_Genre;

    // Constructor
    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    // Method:
    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Les Getters/Setters

    public function getId()
    {
        return $this->_id;
    }


    public function getTitre()
    {
        return $this->_Titre;
    }

    private function setTitre(string $Titre)
    {
        $this->_Titre = htmlentities($Titre);
    }

    public function getData_publi()
    {
        return $this->_Data_publi;
    }

    private function setData_publi($Date_publi)
    {
        $Date_publi = date("d/m/y", $Date_publi);
        $this->_Data_publi = $Date_publi;
    }

    public function getStock()
    {
        return $this->_Stock;
    }

    private function setStock(int $Stock)
    {
        if ($Stock < 0) {
            $Stock = 0;
        }
        $this->_Stock = $Stock;
    }

    public function getResume()
    {
        return $this->_Resume;
    }

    private function setResume(string $Resume)
    {
        $this->_Resume = htmlentities($Resume);
    }

    public function getAuteur()
    {
        return $this->_Auteur;
    }

    private function setAuteur(array $Auteur)
    {
        $this->_Auteur = $Auteur;
    }

    public function getGenre()
    {
        return $this->_Genre;
    }

    private function setGenre(array $Genre)
    {
        $this->_Genre = $Genre;
    }
}

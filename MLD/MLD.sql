#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Books
#------------------------------------------------------------

CREATE TABLE Books(
        id          Int  Auto_increment  NOT NULL ,
        title       Varchar (200) NOT NULL ,
        publication Date NOT NULL ,
        stock       Int NOT NULL ,
        summary     Varchar (50) NOT NULL
	,CONSTRAINT Books_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Users
#------------------------------------------------------------

CREATE TABLE Users(
        id            Int  Auto_increment  NOT NULL ,
        name          Varchar (100) NOT NULL ,
        firstname     Varchar (100) NOT NULL ,
        mail          Varchar (100) NOT NULL ,
        phone_number  Varchar (100) NOT NULL ,
        login         Varchar (100) NOT NULL ,
        password      Varchar (100) NOT NULL ,
        administrator Bool
	,CONSTRAINT Users_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Authors
#------------------------------------------------------------

CREATE TABLE Authors(
        id        Int  Auto_increment  NOT NULL ,
        name      Varchar (100) NOT NULL ,
        firstname Varchar (100) NOT NULL ,
        dates_vie Date NOT NULL
	,CONSTRAINT Authors_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Genres
#------------------------------------------------------------

CREATE TABLE Genres(
        id      Int  Auto_increment  NOT NULL ,
        name    Varchar (100) NOT NULL ,
        summary Varchar (2) NOT NULL
	,CONSTRAINT Genres_PK PRIMARY KEY (id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: RelationLivresAuteurs
#------------------------------------------------------------

CREATE TABLE RelationLivresAuteurs(
        id_Authors       Int NOT NULL ,
        id_Books Int NOT NULL
	,CONSTRAINT RelationLivresAuteurs_PK PRIMARY KEY (id_Authors,id_Books)

	,CONSTRAINT RelationLivresAuteurs_Authors_FK FOREIGN KEY (id_Authors) REFERENCES Authors(id)
	,CONSTRAINT RelationLivresAuteurs_Books0_FK FOREIGN KEY (id_Books) REFERENCES Books(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: RelationLivresGenres
#------------------------------------------------------------

CREATE TABLE RelationLivresGenres(
        id_Genres       Int NOT NULL ,
        id_Books Int NOT NULL
	,CONSTRAINT RelationLivresGenres_PK PRIMARY KEY (id_Genres,id_Books)

	,CONSTRAINT RelationLivresGenres_Genres_FK FOREIGN KEY (id_Genres) REFERENCES Genres(id)
	,CONSTRAINT RelationLivresGenres_Books0_FK FOREIGN KEY (id_Books) REFERENCES Books(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Emprunt
#------------------------------------------------------------

CREATE TABLE Emprunt(
        id_Books          Int NOT NULL ,
        id_Users    Int NOT NULL ,
        date_resa   Date NOT NULL ,
        date_retour Date NOT NULL
	,CONSTRAINT Emprunt_PK PRIMARY KEY (id_Books,id_Users)

	,CONSTRAINT Emprunt_Books_FK FOREIGN KEY (id_Books) REFERENCES Books(id)
	,CONSTRAINT Emprunt_Users0_FK FOREIGN KEY (id_Users) REFERENCES Users(id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Reservation
#------------------------------------------------------------

CREATE TABLE Reservation(
        id_Books        Int NOT NULL ,
        id_Users  Int NOT NULL ,
        Date_resa Date NOT NULL
	,CONSTRAINT Reservation_PK PRIMARY KEY (id_Books,id_Users)

	,CONSTRAINT Reservation_Books_FK FOREIGN KEY (id_Books) REFERENCES Books(id)
	,CONSTRAINT Reservation_Users0_FK FOREIGN KEY (id_Users) REFERENCES Users(id)
)ENGINE=InnoDB;

